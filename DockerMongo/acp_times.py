"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import logging
logging.basicConfig(level=logging.INFO)

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


CONTROL_LOOKUP = {200: {"min": 15, "max": 34},
                  300: {"min": 15, "max": 32},
                  400: {"min": 15, "max": 32},
                  600: {"min": 15, "max": 30},
                  1000: {"min": 11.428, "max": 28}
                  }
KEYS = list(CONTROL_LOOKUP.keys())


def helper(min_or_max, control_dist_km, brevet_dist_km, brevet_start_time):
    time_to_add = 0
    start_time = arrow.get(brevet_start_time)
    dist_remaining = control_dist_km
    count = 0

    for brev_dist in CONTROL_LOOKUP:
        if brev_dist != 200:
            previous_key = KEYS[count - 1]
        if control_dist_km > brev_dist:
            dist_remaining = control_dist_km - brev_dist
            #logging.debug(f"dist remaining: {dist_remaining}")
            if brev_dist != 200:
                time_to_add += (brev_dist - previous_key) / CONTROL_LOOKUP[brev_dist][min_or_max]
                #logging.debug(f"brev_dist: {brev_dist}, time:{time_to_add}")
            else:
                time_to_add += brev_dist / CONTROL_LOOKUP[brev_dist][min_or_max]
                #logging.debug(f"brev_dist: {brev_dist}, time:{time_to_add}")

        else:
            time_to_add += dist_remaining / CONTROL_LOOKUP[brev_dist][min_or_max]
            #logging.debug(f"brev_dist: {brev_dist}, time:{time_to_add}")
            break
        count += 1

    minutes_to_add = round((time_to_add % 1) * 60)
    hours_to_add = int(time_to_add % 24)
    days_to_add = int(time_to_add // 24)
    #logging.debug(f"time added: {days_to_add}:{hours_to_add}:{minutes_to_add} ({time_to_add})")

    control_open_close_time = start_time.shift(days=+days_to_add,
                                           hours=+hours_to_add,
                                           minutes=+minutes_to_add)
    if min_or_max == 'min':
        if control_dist_km == 0:
            current_time = arrow.get(brevet_start_time)
            control_open_close_time = current_time.shift(hours=+1)

        if control_dist_km == 200:
            current_time = arrow.get(brevet_start_time)
            control_open_close_time = current_time.shift(hours=+13, minutes=+30)

    return control_open_close_time.isoformat()

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    return helper('max', control_dist_km, brevet_dist_km, brevet_start_time)


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    return helper('min', control_dist_km, brevet_dist_km, brevet_start_time)
