**Author:** Zane Globus-O'Harra

**Contact:** zfg@uoregon.edu

### Brevet Calculator with a database

This calculates the opening and closing control times for an ACP Brevet.
It takes a brevet distance and control locations. It then outputs the 
opening and closing control times based on the minimum and maximum speeds
laid out in [these rules](https://rusa.org/pages/acp-brevet-control-times-calculator).

Additionally, the times can be submitted to a database when the "Submit" button
is clicked, and can be recalled from the database when "Display" is clicked.